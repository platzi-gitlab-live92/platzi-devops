import type { NextPage } from 'next'
import Link from 'next/link'
import { MainLayout } from '../components/layout/MainLayout';

const ContactPage: NextPage = () => {
  return (
    <MainLayout>
      <h1 className={'title'}>
          Contact Page
      </h1> 
      <p className={'description'}>
          Go to {' '}
          <Link href={`/`} className={'code'}>Home page</Link>
      </p>
    </MainLayout>
  )
}

export default ContactPage


