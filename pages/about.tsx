import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'
import { MainLayout } from '../components/layout/MainLayout'

const AboutPage: NextPage = () => {
  return (
    <MainLayout>
      <h1 className={'title'}>
          About Page
      </h1>

      <p className={'description'}>
          Go to  
          <Link href={`/`} className={'code'}> Home page</Link>
      </p>
    </MainLayout>
  )
}

export default AboutPage
