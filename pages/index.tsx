import type { NextPage } from 'next'
import Link from 'next/link'
import { MainLayout } from '../components/layout/MainLayout';

type HomePageLayout = NextPage & {
  getLayout: (page: JSX.Element) => JSX.Element
}

const HomePage: HomePageLayout = () => {
  return (
    <>
      <h1 className={'title'}>
          HomePage
      </h1>
      <p className={'description'}>
          Go to {' '}
          <Link href={`/about`} className={'code'}>About page</Link>
      </p>
    </>
  )
}

export default HomePage

HomePage.getLayout = function getLayout (page: JSX.Element) {
  return(
    <MainLayout>
      {page}
    </MainLayout>
  )
}
