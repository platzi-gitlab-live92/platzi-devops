import { ActiveLink } from './ActiveLink';
import styles from './Navbar.module.css'

export const Navbar = () => {
  return (
    <nav className={styles.navbar}>
        <ul>
            <li>
                <ActiveLink text='Home' href='/' />
            </li>
            <li>
                <ActiveLink text='About' href='/about' />
            </li>
            <li>
                <ActiveLink text='Contact' href='/contact' />   
            </li>
        </ul>
    </nav>
  )
}
